package
{
	import feathers.controls.List;
	import feathers.data.ListCollection;
	import feathers.layout.CoverFlowLayout;
	import feathers.layout.HorizontalLayout;
	import feathers.themes.MetalWorksMobileTheme;
	import feathers.themes.StyleNameFunctionTheme;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class StarlingRootSprite extends Sprite
	{
		private var _feathersTheme:StyleNameFunctionTheme;
		private var list:List;
		
		public function StarlingRootSprite()
		{
			// Define application UI theme.
			_feathersTheme = new MetalWorksMobileTheme( stage );
			
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event):void
		{
			list = new List();
			addChild(list);
			
			var groceryList:ListCollection = new ListCollection(
				[
					{ label: "Milk" },
					{ label: "Eggs" },
					{ label: "Bread" },
					{ label: "Chicken" },
					{ label: "Milk" },
					{ label: "Eggs" },
					{ label: "Bread" },
					{ label: "Chicken" },
				]);
			list.dataProvider = groceryList;
			
			var listLayout:CoverFlowLayout = new CoverFlowLayout();
			//listLayout.useVirtualLayout = true; //Not Implemented.
			//listLayout.repeatItems = true; // Not Implemented. 
			list.layout = listLayout;
			
			/* 
				Ovo je primjer kako to funkcionira za HorizontalLayout.
				Link na opis implementacije http://wiki.starling-framework.org/feathers/virtual-custom-layouts.
			*/
			var listLayoutTemp:HorizontalLayout = new HorizontalLayout();
			listLayoutTemp.useVirtualLayout = true;
			listLayoutTemp.repeatItems = true;
			
			setLayout();
		}
		
		private function setLayout():void
		{
			list.width = stage.stageWidth;
			list.height = stage.stageHeight;
		}
	}
}