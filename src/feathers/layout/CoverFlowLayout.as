package feathers.layout
{
	import flash.geom.Point;
	
	import feathers.core.IFeathersControl;
	
	import starling.display.DisplayObject;
	import starling.events.EventDispatcher;
	
	public class CoverFlowLayout extends EventDispatcher implements ILayout
	{
		
		private var _horizontalCenterPadding:Number = 25;
		
		public function CoverFlowLayout()
		{
			super();
		}
		
		protected var _requiresLayoutOnScroll:Boolean = true;
		
		private var gap:Number;
		private var _lateralCopiesCount:Number = 1;
		
		private var _minimalRatio:Number = 0.5;
		
		/**
		 * Minimal scale and alpha value that can be applied on items. Default value is 0.5.
		 */
		public function get minimalRatio():Number
		{
			return _minimalRatio;
		}
		
		public function set minimalRatio(value:Number):void
		{
			_minimalRatio = value;
		}
		
		
		public function get requiresLayoutOnScroll():Boolean
		{
			return _requiresLayoutOnScroll;
		}
		
		public function layout(items:Vector.<DisplayObject>, viewPortBounds:ViewPortBounds=null, result:LayoutBoundsResult=null):LayoutBoundsResult
		{
			// initialize the view port's position and dimensions
			var startX:Number = 0;
			var startY:Number = 0;
			var explicitWidth:Number = NaN;
			var explicitHeight:Number = NaN;
			var minWidth:Number = 0;
			var minHeight:Number = 0;
			var maxWidth:Number = Number.POSITIVE_INFINITY;
			var maxHeight:Number = Number.POSITIVE_INFINITY;
			if(viewPortBounds)
			{
				startX = viewPortBounds.x;
				startY = viewPortBounds.y;
				explicitWidth = viewPortBounds.explicitWidth;
				explicitHeight = viewPortBounds.explicitHeight;
				minWidth = viewPortBounds.minWidth;
				minHeight = viewPortBounds.minHeight;
				maxWidth = viewPortBounds.maxWidth;
				maxHeight = viewPortBounds.maxHeight;
			}
			
			// loop through the items and position them
			var positionX:Number = startX;
			var maxItemHeight:Number = 0;
			var itemCount:int = items.length;
			for(var i:int = 0; i < itemCount; i++)
			{
				var item:DisplayObject = items[i];
				// skip items that aren't included in the layout
				var layoutItem:ILayoutDisplayObject = item as ILayoutDisplayObject;
				if(layoutItem && !layoutItem.includeInLayout)
				{
					continue;
				}
				// special case for Feathers components
				if(item is IFeathersControl)
				{
					IFeathersControl(item).validate();
				}
				
				
				// reset scale
				item.scaleX = item.scaleY = 1;
				
				if(i==0){
					gap = (viewPortBounds.explicitWidth - item.width)*0.5;
					item.x = positionX + gap;
					positionX += gap;
				}else{				
					item.x = positionX;
				}
				item.height = explicitHeight - this._horizontalCenterPadding * 2;
				
				positionX += item.width;
				
				// used for the final content width below
				maxItemHeight = Math.max(maxItemHeight, item.height);
				
				var halfScreen:Number = explicitWidth * 0.5;
				var itemGlobalPosition:Number = item.x + item.parent.x + (item.width * 0.5);
				var ratio:Number = 0;
				var tempWidth:Number = item.width;
				if(itemGlobalPosition <= halfScreen)
				{
					ratio = itemGlobalPosition / halfScreen;
					ratio = convertRange(0, 1, 0.5, 1, ratio);
					item.scaleX = item.scaleY = ratio;
					item.alpha = ratio;
					item.x = item.x + (tempWidth - item.width) * 0.5;
				}
				else{
					ratio = (explicitWidth - itemGlobalPosition) / halfScreen;
					ratio = convertRange(0, 1, _minimalRatio, 1, ratio);
					item.scaleX = item.scaleY = (ratio >= _minimalRatio)? ratio : _minimalRatio;
					item.alpha = ratio;
					item.x = item.x + (tempWidth - item.width) * 0.5;
				}
				item.y = (explicitHeight - item.height) * 0.5;
			}
			
			// used for the final content height below
			positionX -= startX;
			positionX += gap;
			
			// prepare the result object and return it
			if(!result)
			{
				result = new LayoutBoundsResult();
			}
			var viewPortWidth:Number = explicitWidth;
			var viewPortHeight:Number = explicitHeight;
			if(viewPortWidth != viewPortWidth) //isNaN
			{
				viewPortWidth = Math.max(minWidth, Math.min(maxWidth, positionX));
			}
			if(explicitHeight != explicitHeight) //isNaN
			{
				viewPortHeight = Math.max(minHeight, Math.min(maxHeight, maxItemHeight));
			}
			var contentWidth:Number = Math.max(positionX, viewPortWidth);
			var contentHeight:Number = Math.max(maxItemHeight, viewPortHeight);
			result.viewPortWidth = viewPortWidth;
			result.viewPortHeight = viewPortHeight;
			result.contentWidth = contentWidth;
			result.contentHeight = contentHeight;
			return result;
		}
		
		public function getScrollPositionForIndex(index:int, items:Vector.<DisplayObject>, x:Number, y:Number, width:Number, height:Number, result:Point=null):Point
		{
			// loop through the items to calculate the scroll position
			var positionX:Number = 0;
			for(var i:int = 0; i < index; i++)
			{
				var item:DisplayObject = items[i];
				var layoutItem:ILayoutDisplayObject = item as ILayoutDisplayObject;
				if(layoutItem && !layoutItem.includeInLayout)
				{
					continue;
				}
				if(item is IFeathersControl)
				{
					IFeathersControl(item).validate();
				}
				positionX += item.width / item.scaleX;
			}
			
			// prepare the result object and return it
			if(!result)
			{
				result = new Point();
			}
			result.x = positionX;
			result.y = 0;
			return result;
		}
		
		private function convertRange(oldMin:Number, oldMax:Number, newMin:Number, newMax:Number, oldValue:Number):Number
		{
			var newValue:Number;
			var newRange:Number;
			var oldRange:Number = (oldMax - oldMin)
			if (oldRange == 0)
				newValue = newMin
			else
			{
				newRange = (newMax - newMin)  
				newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin
			}
			return newValue;
		}
		
		public function getNearestScrollPositionForIndex(index:int, scrollX:Number, scrollY:Number, items:Vector.<DisplayObject>, x:Number, y:Number, width:Number, height:Number, result:Point=null):Point
		{
			// TODO Auto Generated method stub
			return null;
		}
		
		
	}
}


