package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import feathers.system.DeviceCapabilities;
	
	import starling.core.Starling;
	
	[SWF(frameRate="60", width="405", height="720")]
	public class FeathersCoverFlowLayout extends Sprite
	{
		protected var _starling:Starling;
		
		public function FeathersCoverFlowLayout()
		{
			super();
			
			// Init stage.
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.frameRate = 60;
			
			// No flash.display mouse interactivity.
			mouseEnabled = mouseChildren = false;
			
			// Init Starling.
			Starling.handleLostContext = true;
			Starling.multitouchEnabled = true;
			_starling = new Starling( StarlingRootSprite, stage, null, null, "auto", "auto" );
			_starling.enableErrorChecking = false;
			_starling.showStats = true;
			_starling.simulateMultitouch = true;
			_starling.start();
			
			// Simulate screen properties.
			DeviceCapabilities.dpi = 120;
			DeviceCapabilities.screenPixelWidth = 1080;
			DeviceCapabilities.screenPixelHeight = 1920;
			
			// Update Starling view port on stage resizes.
			stage.addEventListener( Event.RESIZE, onStageResize, false, int.MAX_VALUE, true );
		}
		
		
		
		private function onStageResize( event:Event ):void {
			if( stage.stageWidth < 256 || stage.stageHeight < 256 ) return;
			_starling.stage.stageWidth = stage.stageWidth;
			_starling.stage.stageHeight = stage.stageHeight;
			const viewPort:Rectangle = _starling.viewPort;
			viewPort.width = stage.stageWidth;
			viewPort.height = stage.stageHeight;
			try {
				_starling.viewPort = viewPort;
			}
			catch( e:Error ) {
				trace(e);
			}
		}
	}
}